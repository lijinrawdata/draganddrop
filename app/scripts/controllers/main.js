'use strict';

/**
 * @ngdoc function
 * @name draganddropApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the draganddropApp
 */
angular.module('draganddropApp')
  .controller('MainCtrl', function () {
    var main = this;

    main.list = [
    			{name:'First',elements:[{name:'First 1'},{name:'First 2'},{name:'First 3'},{name:'First 4'},{name:'First 5'},{name:'First 6'},{name:'First 7'},{name:'First 8'},{name:'First 9'}]},
    			{name:'Second',elements:[{name:'Second 1'},{name:'Second 2'},{name:'Second 3'},{name:'Second 4'}]},
    			{name:'Third',elements:[{name:'Third 1'},{name:'Third 2'}]}
    			];
    main.moved = function (index,element,section){
    	section.elements.splice(index, 1);
    	element.moved=true;
    };
  });
