'use strict';

/**
 * @ngdoc function
 * @name draganddropApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the draganddropApp
 */
angular.module('draganddropApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
